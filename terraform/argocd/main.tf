resource "helm_release" "argocd" {
  name             = "argocd"
  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  namespace        = "argocd"
  create_namespace = true
  version          = "6.7.1"
  values           = [file("argocd/values/argocd.yaml")]
}

resource "helm_release" "root-app" {
  name             = "root-app"
  repository       = "./charts"
  chart            = "root-app"
  namespace        = "argocd"
  depends_on = [ helm_release.argocd ]
}

#!/bin/sh

# crap out on error
set -e

# this is for the mac only with brew
# for linux use the your method for installing minikube
# https://minikube.sigs.k8s.io/docs/start/

# terraform and kubectl and jq needs to be installed

mk="minikube"
tf="terraform"
v="v1.30.0"

install_mk () {
    if which "$mk" > /dev/null 2>&1; then
        echo "starting $mk $v"
        "$mk" start --driver=docker --kubernetes-version="$v"
        "$mk" addons enable ingress
    else
        echo "installing $mk $v"
        if [ "$(uname)" = "Linux" ]; then
            yay -Sy --noconfirm install minikube
            echo "starting $mk $v"
            "$mk" start --driver=docker --kubernetes-version="$v" && \
            "$mk" addons enable ingress
        elif brew install "$mk"; then
            echo "starting "$mk" $v"
            "$mk" start --driver=docker --kubernetes-version="$v" && \
            "$mk" addons enable ingress
        else
            echo "failed to install and start $mk"
        fi
    fi
}

tf () {
    echo "installing argocd with terraform on minikube"
    cd ./"$tf"
    if "$tf" init; then
        if "$tf" plan -out .terraformplan.out; then
            "$tf" apply -auto-approve .terraformplan.out
        else
            echo "$tf plan failed"
        fi
    else
        echo "$tf init failed"
    fi
    cd -
}

install_mk
# give minikube time to start
# TODO: add a check to see if ingress is ready
sleep 30
tf

echo "getting first time initial password username:admin"
if secret=$(kubectl get secrets -n argocd argocd-initial-admin-secret -o json | \
    jq -r .data.password | base64 -d); then
    echo "$secret"
    echo "$mk service -n argocd argocd-server --url"
else 
    echo "can't get init password"
fi






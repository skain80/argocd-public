#!/bin/sh

# crap out on error
set -e


# remove existing tunnels
prep() {
    echo "preparing and cleaning up existing tunnels"
    # check if /etc/hosts has registry entry
    if ! grep -q "registry" /etc/hosts; then
        # add registry entry to /etc/hosts
        echo "127.0.0.1 registry" | sudo tee -a /etc/hosts > /dev/null
    fi
    if ! grep -q "jenkins" /etc/hosts; then
        # add jenkins entry to /etc/hosts
        echo "127.0.0.1 jenkins" | sudo tee -a /etc/hosts > /dev/null
    fi
    if ! grep -q "argocd" /etc/hosts; then
        # add argo entry to /etc/hosts
        echo "127.0.0.1 argocd" | sudo tee -a /etc/hosts > /dev/null
    fi

    sudo pkill -f ".*docker.*80.*80.*" 2>/dev/null || true
    sudo pkill -f ".*docker.*443.*443.*" 2>/dev/null || true
}

# get ssh port

get_variables () {
    sshport=""
    while [ -z "$sshport" ]; do
        echo "waiting for ssh port"
        sshport=$(docker ps | grep minikube | sed -n 's/.*\([0-9]\{5\}\)->22.*/\1/p' 2>/dev/null)
        sleep 1
    done
    rsa=$(minikube ssh-key)
    ingress_ip=$(kubectl get service -A | grep ingress-nginx-controller | grep -i nodeport | awk '{print $4}' 2>/dev/null)

}

expose(){
    echo "exposing ingress on port 80/443"
    sudo nohup ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o IdentitiesOnly=yes -N docker@127.0.0.1 -p "$sshport" -i"$rsa" -L 80:"$ingress_ip":80 > /dev/null &
    sudo nohup ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o IdentitiesOnly=yes -N docker@127.0.0.1 -p "$sshport" -i"$rsa" -L 443:"$ingress_ip":443 > /dev/null &
}

prep
get_variables
expose
